(ns ircbot.commands
  (:require [clojure.string :as str]
            [irclj.core :as irc]
            [ircbot.parsing :as parse]
            [ircbot.delegation :as delegation]
            [ircbot.config :refer :all]))

(defn quit
  "Kill irc connection and exit"
  [_ type]
  (irc/kill @connection)
  (System/exit 0))

(defn join
  "Join all listed channels"
  [_ type]
  (prn (parse/get-args type))
  (run! (partial irc/join @connection) (parse/get-args type)))

(defn part
  "Part all listed channels"
  [_ type]
  (run! (partial irc/part @connection) (parse/get-args type)))

(defn help
  "Print a help message, or give information about a delegated command"
  [irc type]
  (let [command (first (parse/get-args type))]
    (if (nil? command)
      (irc/message @connection out-channel (str
                                            "I try to send a command to another "
                                            "bot that hangs out here. Try help "
                                            "<command> for a specific command."))
      (if (contains? delegation/delegated-commands command)
        (do
          (irc/message @connection out-channel
                       (str "The delegation preference order for this command "
                            "is " (print-str (get delegation/delegated-commands
                                                  command))))
          (delegation/delegate
           irc
           (update type :text #(str/replace % #"help " ""))
           :pre "help "))
        (irc/message @connection out-channel (str "Command not recognised, "
                                                  "sorry!"))))))

(defn source
  "Provide a link to the source code"
  [& _]
  (irc/message @connection out-channel
               "https://gitlab.com/lunik1/DelegatorBot"))

(def builtin-commands
  {"quit" quit
   "join" join
   "part" part
   "help" help
   "source" source})
