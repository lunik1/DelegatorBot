(ns ircbot.config)

(def command-char \:)

(def server "irc.dbcommunity.org")

(def port 6667)

(def nick "DelegatorBot")

(def out-channel
  "Channel to pipe all bot responses too"
  "#desertbus")

(def connection (promise))
