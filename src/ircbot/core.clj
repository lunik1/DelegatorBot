(ns ircbot.core
  (:require [clojure.string :as str]
            [irclj.core :as irc]
            [ircbot.parsing :as parse]
            [ircbot.commands :as cmd]
            [ircbot.config :refer :all]
            [ircbot.delegation :refer :all])
  (:gen-class))

(defn from-bot?
  "Is a PRIVMSG from a bot?"
  [type]
  (contains? bot-prefixes (type :nick)))

(defn privmsg-callback
  "Function called every time a PRIVMSG is received from irc"
  [irc type & s]
  ;; (prn irc)
  ;; (prn type)
  ;; (prn s)
  (if (from-bot? type)
    (when (= nick (type :target))
      (irc/message @connection out-channel (type :text))) ;; relay bot responses
    (when-let [command (and (parse/command? type)
                            (parse/get-command type))]
      (cond
        (contains? cmd/builtin-commands command)
        ((get cmd/builtin-commands command) irc type)

        (contains? delegated-commands command)
        (when (= out-channel (type :target))
          (delegate irc type))))))

(defn -main
  []
  (deliver connection (irc/connect server port nick
                                   :callbacks {:privmsg privmsg-callback}))
  (irc/join @connection out-channel))
