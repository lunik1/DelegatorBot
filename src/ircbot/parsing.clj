(ns ircbot.parsing
  (:require [clojure.string :as str]
            [ircbot.config :refer :all]))

(defn command?
  "Return true if the message is a bot command, i.e. a PRIVMSG beginning with
  the command-char"
  [type]
  (and
   (= (type :command) "PRIVMSG")
   (= (first (type :text)) command-char)))

(defn split-words
  "Split s by whitespace"
  [s]
  (str/split s #"\s+"))

(defn get-command
  "Get the name of a bot command from type"
  [type]
  (subs (first (split-words (type :text))) 1))

(defn get-args
  "Get arguments of a bot command from type"
  [type]
  (rest (split-words (type :text))))
