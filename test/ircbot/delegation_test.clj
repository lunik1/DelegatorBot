(ns ircbot.delegation-test
  (:require [ircbot.delegation :refer :all]
            [ircbot.config :refer :all]
            [clojure.test :refer :all]))

(def fake-irc (atom {:channels {out-channel {:users {"user1" nil
                                                     "ekimbot" nil
                                                     "Hubbot" nil
                                                     "user2" nil
                                                     "DideRobot" nil
                                                     "user3" nil}}}}))

(deftest first-availible-bot-test
  (is (= "DideRobot" (first-availible-bot fake-irc "say")))
  (is (nil? (first-availible-bot fake-irc "ud"))))

(deftest change-prefix-test
  (is (= "@say" (change-prefix "ekimbot" ":say"))))
