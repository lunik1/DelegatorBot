(ns ircbot.parsing-test
  (:require [ircbot.parsing :refer :all]
            [clojure.test :refer :all]))

(def privmsg {:command "PRIVMSG" :text "as is tradition"})

(def command-quit {:command "PRIVMSG" :text ":quit"})

(def command-join {:command "PRIVMSG" :text ":join #alpha #beta    #zeta"})

(def command-none {:command "PRIVMSG" :text ":"})

(def command-bad {:command "PRIVMSG" :text ":regjgr"})

(def notice {:command "NOTICE" :text ":quit"})

(deftest command?-test
  (is (true? (command? command-quit)))
  (is (true? (command? command-join)))
  (is (true? (command? command-none)))
  (is (false? (command? privmsg)))
  (is (false? (command? notice))))

(deftest split-words-test
  (is (= [":join" "#alpha" "#beta" "#zeta"] (split-words (command-join :text))))
  (is (= [":quit"] (split-words (command-quit :text)))))

(deftest get-command-test
  (is (= "quit" (get-command command-quit)))
  (is (= "join" (get-command command-join))))

(deftest get-args-test
  (is (= '("#alpha" "#beta" "#zeta") (get-args command-join)))
  (is (= '() (get-args command-quit))))
